/** import packages starts here **/
import React from "react";
import ReviewRating from "./components/ReviewRating";
/** import packages ends here **/

const Review = (props) => {
  /**
   * destructuring props
   */
  let {
    data,
    mainClass,
    onChange,
    onSubmit,
    submitBtnIcon,
    reviewBtnClass,
    textAreaClass,
    textAreaValue,
    textAreaName,
    textAreaPlaceholder,
    starCount,
    imgStyle,
    onRatingClick,
    reviewDescription,
    ratingClass,
    btnText,
  } = props;

  return (
    <>
      <div className={`row border p-2 ${mainClass}`}>
        <div className="col-2 col-md-1 text-end">
          <img src={data?.img} alt="user_img_avatar" style={imgStyle} />
        </div>
        <div className="col-10 col-md-11 d-flex flex-column">
          <strong>{data?.username}</strong>
          <span className="mt-1">
            {reviewDescription || "Reviews are public. Post your review here"}
          </span>
        </div>
        <div className="col-12">
          <div className={`component text-center ${ratingClass}`}>
            <ReviewRating starCount={starCount} onRatingClick={onRatingClick} />
          </div>
        </div>
        <div className="col-md-1"></div>
        <div className="col-md-11">
          <textarea
            name={`${textAreaName || "review"}`}
            id=""
            rows="6"
            cols="30"
            className={`${textAreaClass}`}
            placeHolder={textAreaPlaceholder || "Describe your review...."}
            value={textAreaValue}
            onChange={(e) => onChange(e)}
          ></textarea>
          <div className="mt-2">
            <button className={`${reviewBtnClass}`} onClick={onSubmit}>
              {submitBtnIcon && <span className="me-1">{submitBtnIcon}</span>}
              {btnText || "Review"}
            </button>
          </div>
        </div>
      </div>
    </>
  );
};

export default Review;
