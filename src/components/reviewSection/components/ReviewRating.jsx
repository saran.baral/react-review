/** import packages starts here **/
import React, { useState } from "react";
/** import packages ends here **/

const ReviewRating = ({ starCount = 5, onRatingClick }) => {
  const [rating, setRating] = useState(0);
  const [hover, setHover] = useState(0);

  /**
   * Function for handling rating click events and pass arg to parent function
   * @param type
   * @returns {*}
   */

  function userRating(val) {
    setRating(val);
    onRatingClick(val);
  }

  return (
    <>
      {Array(starCount)
        .fill({})
        .map((x, i) => {
          i += 1;
          return (
            <svg
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 35 35"
              width="40"
              height="40"
              onClick={() => userRating(i)}
              onMouseEnter={() => setHover(i)}
              onMouseLeave={() => setHover(rating)}
              style={{ cursor: "pointer" }}
            >
              <path fill="none" d="M0 0h24v24H0z" />
              <path
                d={
                  i <= (hover || rating)
                    ? "M12 18.26l-7.053 3.948 1.575-7.928L.587 8.792l8.027-.952L12 .5l3.386 7.34 8.027.952-5.935 5.488 1.575 7.928z"
                    : "M12 18.26l-7.053 3.948 1.575-7.928L.587 8.792l8.027-.952L12 .5l3.386 7.34 8.027.952-5.935 5.488 1.575 7.928L12 18.26zm0-2.292l4.247 2.377-.949-4.773 3.573-3.305-4.833-.573L12 5.275l-2.038 4.42-4.833.572 3.573 3.305-.949 4.773L12 15.968z"
                }
              />
            </svg>
          );
        })}
    </>
  );
};

export default ReviewRating;
