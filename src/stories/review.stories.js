import React from "react";
import { Review } from "../components";

export default {
  title: "Bootstrap - UI Components/Review",
  component: Review,
};

let data = {
  rating: 4,
  recommend: "yes",
  reviewable_type: "movies",
  review: "this movie was great!I enjoyed it.",
  content_slug: "mad-max-fury-road",
  reviewable_id: "456",
  user_id: "1725462",
  username: "mindfog",
};

const imgStyle = {
  padding: "2px",
  maxWidth: "50px",
  objectFit: "cover",
  borderRadius: "50%",
};

const Template = (args) => <Review {...args} />;

export const ReviewComponent = Template.bind({});

function onChange(e) {
  console.log(e.target.value);
}

function onSubmit(e) {
  alert("submit pressed");
}

function onRatingClick(e) {
  alert(e);
}

ReviewComponent.args = {
  data: data,
  imgStyle,
  starCount: 6,
  submitBtnIcon: "👌",
  reviewBtnClass: "btn btn-outline-primary btn-sm float-end",
  textAreaClass: "mt-1 w-100 form-control ",
  btnText: "Submit Review",
  onSubmit,
  onChange,
  onRatingClick,
};
